libtest-www-mechanize-catalyst-perl (0.62-2) UNRELEASED; urgency=medium

  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Feb 2020 16:46:18 +0100

libtest-www-mechanize-catalyst-perl (0.62-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ gregor herrmann ]
  * Import upstream version 0.62.
  * Declare compliance with Debian Policy 4.3.0.
  * Drop unneeded version contraint from (build) dependency.
  * Bump debhelper compatibility level to 11.
  * Remove trailing whitespace from debian/*.
  * Disable https?_proxy env var for autopkgtest's smoke test like during
    build.

 -- gregor herrmann <gregoa@debian.org>  Thu, 21 Feb 2019 21:22:42 +0100

libtest-www-mechanize-catalyst-perl (0.60-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Drop xz compression for {binary,source} package, set by default by
    dpkg since 1.17.{0,6}.
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata.
  * Import upstream version 0.60.
  * Update upstream contact in d/copyright.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Fri, 07 Aug 2015 16:02:45 +0200

libtest-www-mechanize-catalyst-perl (0.59-1) unstable; urgency=medium

  * New upstream release.
  * Add (build) dependency on libclass-load-perl.
  * Drop Breaks/Replaces on ancient libcatalyst-modules-perl.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Wed, 15 Jan 2014 22:13:40 +0100

libtest-www-mechanize-catalyst-perl (0.58-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Xavier Guimard ]
  * New upstream version 0.58
  * Update dependencies :
    + libcatalyst-perl >= 5.90000
  * Add myself to uploaders
  * Bump Standards-Version to 3.9.4
  * Update debian/copyright (years)

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

 -- Xavier Guimard <x.guimard@free.fr>  Sun, 12 May 2013 18:32:32 +0200

libtest-www-mechanize-catalyst-perl (0.57-1) unstable; urgency=low

  * Add a Pre-Depends on dpkg (>= 1.15.6~) for the xz compression.

  * New upstream release.
  * debian/copyright: update to Copyright-Format 1.0.
  * Bump Standards-Version to 3.9.3 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Mon, 23 Apr 2012 16:22:00 +0200

libtest-www-mechanize-catalyst-perl (0.56-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Use XZ compression for source and binary packages.

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 18 Oct 2011 20:41:25 +0200

libtest-www-mechanize-catalyst-perl (0.55-1) unstable; urgency=low

  * New upstream release.
  * Bump debhelper compatibility level to 8.

 -- gregor herrmann <gregoa@debian.org>  Fri, 30 Sep 2011 17:37:01 +0200

libtest-www-mechanize-catalyst-perl (0.54-1) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * New upstream release.
  * debian/copyright: Update years of copyright for inc/Module/*.
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Ansgar Burchardt <ansgar@debian.org>  Wed, 10 Aug 2011 12:53:17 +0200

libtest-www-mechanize-catalyst-perl (0.53-2) unstable; urgency=low

  * Add build dependency on libtest-exception-perl (closes: #615500).

 -- gregor herrmann <gregoa@debian.org>  Sun, 27 Feb 2011 16:04:36 +0100

libtest-www-mechanize-catalyst-perl (0.53-1) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * Set Standards-Version to 3.9.1; replace Conflicts with Breaks.

  [ Ansgar Burchardt ]
  * New upstream release.
  * debian/copyright: Refer to /usr/share/common-licenses/GPL-1; refer to
    "Debian systems" instead of "Debian GNU/Linux systems".
  * Use source format 3.0 (quilt).

 -- Ansgar Burchardt <ansgar@debian.org>  Thu, 16 Dec 2010 13:34:53 +0100

libtest-www-mechanize-catalyst-perl (0.52-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Standards-Version 3.8.4 (no changes)
  * Use new short debhelper rules format
  * Rewrite control description
  * Now depend on WWW::Mechanize 1.54 per upstream
  * Add myself to Uploaders and Copyright
  * Refresh copyright to new DEP5 format

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ gregor herrmann ]
  * Update years of upstream copyright.

 -- Jonathan Yu <jawnsy@cpan.org>  Sat, 13 Mar 2010 23:36:30 -0500

libtest-www-mechanize-catalyst-perl (0.51-2) unstable; urgency=low

  * debian/control:
    - make the Conflicts: on libcatalyst-modules-perl versioned
      and add a Replaces:, so that Test::WWW::Mechanize::Catalyst can "move" to
      this new package
    - add /me to Uploaders
    - add build dependencies on libcatalyst-modules-perl (required for tests)
      and libtest-pod-perl (enabled an additional test)
  * Don't install README anymore (text version of the POD).
  * debian/copyright: mention the copyright holder as specified in the source.
  * debian/rules: unset http_proxy.

 -- gregor herrmann <gregoa@debian.org>  Thu, 16 Apr 2009 18:19:34 +0200

libtest-www-mechanize-catalyst-perl (0.51-1) unstable; urgency=low

  * New upstream release
  * debian/control: Standards-Version updated to 3.8.1 (no changes)

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Tue, 17 Mar 2009 10:43:06 +0100

libtest-www-mechanize-catalyst-perl (0.50-1) unstable; urgency=low

  * Initial Release (closes: #517852)

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Mon, 2 Mar 2009 11:54:12 +0100
